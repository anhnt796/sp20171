<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=utf-8"/>
    <title>Market To Home</title>
    <style type="text/css">
        <!--
        a {
            color: #3399FF
        }

        .topmenu {
            font-family: Arial, Helvetica, sans-serif;
            font-style: normal;
            color: #FFFFFF;
        }

        .style1 {
            color: #CC6600
        }

        .style2 {
            color: #0099FF
        }

        -->
    </style>
</head>

<body>
<div align="center">
    <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="3" valign="top"><?php require("top.php"); ?></td>
        </tr>
        <tr>
            <td width="200" valign="top"><?php require("left.php"); ?></td>
            <td width="548" valign="top">
                <!--begin body-->
                <form name="searchform" method="post" action="searchresult.php">
                    <table width="100%" border="0" cellspacing="1" cellpadding="1">
                        <tr>
                            <td width="13%">Từ khóa</td>
                            <td width="15%"><label><input type="text" name="keyword" id="keyword"></label></td>
                            <td width="13%">Loại sách</td>
                            <td width="15%">
                                <select name="category" id="category">
                                    <?php
                                    require("connection.php");
                                    $select = "select * from Categories";
                                    $result = mysqli_query($connect, $select);
                                    echo "<option value=''>Chọn loại sách</option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo "<option value=";
                                        echo $row["CategoryId"];
                                        echo ">" . $row["CategoryName"];
                                        echo "</option>";
                                    }
                                    require("closeconnection.php");
                                    ?>
                                </select>
                            </td>
                            <td><input type="submit" name="quick_submit" value="Tìm kiếm"></td>
                        </tr>
                        <input name="searchfrom" type="hidden" id="searchfrom" value="quick">
                    </table>
                </form>
                <br>
                <a href="basicsearch.php">Tìm kiếm cơ bản</a>
                <br>
                <a href="advancedsearch.php">Tìm kiếm nâng cao</a>

                <!--end body--></td>
            <td width="242" valign="top"><?php require("right.php"); ?></td>
        </tr>
        <tr>
            <td colspan="3" valign="top"><?php include("bottom.html"); ?></td>
        </tr>
    </table>
</div>
</body>
</html>
