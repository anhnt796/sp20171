<?php
class Database
{
    var $servername = "localhost";
    var $db_username = "root";
    var $db_password = "";
    var $db_name = "marketohome";

    function get_servername() {
        return $this->servername;
    }

    function get_username() {
        return $this->db_username;
    }

    function get_password() {
        return $this->db_password;
    }

    function get_databasename() {
        return $this->db_name;
    }

    function getConnection() {
        $connect = mysqli_connect($this->servername, $this->db_username, $this->db_password);
        if (!$connect) {
            die('Could not connect MySQL: ' . mysqli_error($connect));
        }
        return $connect;
    }

    function closeConnection() {
        mysqli_close($this->connect);
    }

    function getResultSet($select) {
        $this->connect = $this->getConnection();
        mysqli_select_db($this->connect(), $this->db_name);
        mysqli_query($this->connect(), "SET NAMES 'utf8'");
        $result = mysqli_query($this->connect(), $select);
        return $result;
    }
    // design pattern singleton
//    private static $_instance = null;
//
//    public static function getInstance()
//    {
//        if (!isset(self::$_instance)) {
//            $self::$_instance = new Database();
//        }
//        return $self::_instance;
//    }
}