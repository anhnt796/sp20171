<?php
require("db_login.php");
$connect = mysqli_connect($servername, $db_username, $db_password);
if (!$connect) {
    die('Could not connect MySQL: ' . mysqli_error($connect));
} else {
    mysqli_select_db($connect, $db_name);
    mysqli_query($connect, "SET NAMES 'utf8'");
}
?>