<?php
session_start();
if (isset($_SESSION["UserId"]))
    header("Location:myaccount.php");
$message = "Enter your username and password:";
if (isset($_SESSION["Wrong"])) {
    $wrongCode = $_SESSION["Wrong"];
    if ($wrongCode == "1") {
        $message = "<font color='red'>User name is not existing, please enter again:</font>";
    }
    if ($wrongCode == "2") {
        $message = "<font color='red'>Wrong password, please enter again:</font>";
    }
}
?>

<form action='login-authentication.php' method='post'>
    <?php
    echo $message;
    ?>
    <br>
    <table>
        <tr>
            <td>User name:</td>
            <td><input type='text' name='user' required></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='pass' required></td>
        </tr>
        <tr>
            <td></td>
            <td><input type='submit' name='submit' value='OK'><input type='reset' name='reset' value='Reset'></td>
        </tr>
    </table>
</form>