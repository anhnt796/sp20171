<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=utf-8"/>
    <title>Market To Home</title>
    <style type="text/css">
        <!--
        a {
            color: #3399FF
        }

        .topmenu {
            font-family: Arial, Helvetica, sans-serif;
            font-style: normal;
            color: #FFFFFF;
        }

        .style1 {
            color: #CC6600
        }

        .style2 {
            color: #0099FF
        }

        -->
    </style>
</head>

<body>
<div align="center">
    <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="3" valign="top"><?php require("top.php"); ?></td>
        </tr>
        <tr>
            <td width="200" valign="top"><?php require("left.php"); ?></td>
            <td width="548" valign="top">
                <!--begin body-->
                <?php
                $whereclause = "";
                $searchpage = $_POST["searchfrom"];
                switch ($searchpage) {
                    case "quick":
                        $keyword = $_POST["keyword"];
                        if ($keyword != "") {
                            $whereclause = " and (ItemName like '%" . replace($keyword) . "%' or Description like '%" . replace($keyword) . "%')";
                        }
                        $category = $_POST["category"];
                        if ($category != "") {
                            $whereclause .= " and CategoryId='";
                            $whereclause .= $category . "'";
                        }
                        break;
                    case "basic":
                        $keyword = $_POST["keyword"];
                        if ($keyword != "") {
                            $whereclause = " and (ItemName like '%" . replace($keyword) . "%' or Description like '%" . replace($keyword) . "%')";
                        }
                        $category = $_POST["category"];
                        if ($category != "") {
                            $whereclause .= " and CategoryId='";
                            $whereclause .= $category . "'";
                        }
                        $author = $_POST["author"];
                        if ($author != "") {
                            $whereclause .= " and AuthorId='";
                            $whereclause .= $author . "'";
                        }
                        $publisher = $_POST["publisher"];
                        if ($publisher != "") {
                            $whereclause .= " and PublisherId='";
                            $whereclause .= $publisher . "'";
                        }
                        break;
                    case "advanced":
                        $keyword = $_POST["keyword"];
                        if ($keyword != "") {
                            $whereclause = " and (ItemName like '%" . replace($keyword) . "%' or Description like '%" . replace($keyword) . "%')";
                        }
                        $category = $_POST["category"];
                        if ($category != "") {
                            $whereclause .= " and CategoryId='";
                            $whereclause .= $category . "'";
                        }
                        $author = $_POST["author"];
                        if ($author != "") {
                            $whereclause .= " and AuthorId='";
                            $whereclause .= $author . "'";
                        }
                        $publisher = $_POST["publisher"];
                        if ($publisher != "") {
                            $whereclause .= " and PublisherId='";
                            $whereclause .= $publisher . "'";
                        }
                        $readertype = $_POST["readertype"];
                        if ($readertype != "") {
                            $whereclause .= " and ReaderTypeId='";
                            $whereclause .= $readertype . "'";
                        }
                        $language = $_POST["language"];
                        if ($language != "") {
                            $whereclause .= " and LanguageId='";
                            $whereclause .= $language . "'";
                        }
                        $minprice = $_POST["minprice"];
                        if ($minprice != "") {
                            $whereclause .= " and Price>='";
                            $whereclause .= $minprice . "'";
                        }
                        $maxprice = $_POST["maxprice"];
                        if ($maxprice != "") {
                            $whereclause .= " and Price>='";
                            $whereclause .= $maxprice . "'";
                        }
                        break;
                }

                function replace($string)
                {
                    return str_replace("'", "''", $string);
                }

                function int_divide($x, $y)
                {
                    if ($x == 0) {
                        return 0;
                    }
                    if ($y == 0) {
                        return FALSE;
                    }
                    return ($x - $x % $y) / $y;
                }

                ?>

                <b>KẾT QUẢ TÌM KIẾM</b>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <?php
                        require("connection.php");
                        $select = "select * from Items where 1=1" . $whereclause;
                        $result = mysqli_query($connect, $select);
                        $rows = 0;
                        if ($result != null) {
                            $rows = mysqli_num_rows($result);
                            $i = 0;
                            while ($row = mysqli_fetch_array($result)) {
                                if ($i == 0) {
                                    echo "<td valign='top' width='50%'>";
                                    echo "<table width='100%' border='0' cellspacing='2' cellpadding='2'>";
                                }
                                if (int_divide($rows, 2) == $i) {
                                    echo "</table></td>";
                                    echo "<td valign='top' width='50%'>";
                                    echo "<table width='100%' border='0' cellspacing='2' cellpadding='2'>";
                                }
                                echo "<tr>";
                                echo "<td rowspan='4' valign='top'>";
                                echo "<img width='80px' src='";
                                echo $row["ItemId"] . ".jpg'></td>";
                                echo "<td valign='top'><a href='details.php?id=" . $row["ItemId"] . "'>" . $row["ItemName"] . "</a></td>";
                                echo "</tr>";
                                echo "<tr><td valign='top'>Price: " . $row["Price"] . "</td></tr>";
                                echo "<tr><td valign='top'>Size: " . $row["Size"] . "</td></tr>";
                                echo "<tr><td valign='top'>Weight: " . $row["Weight"] . "</td></tr>";
                                $i++;
                            }
                            echo "</table></td></tr>";
                            echo "<tr><td valign='top'><b>Số mẩu tin tìm thấy: " . $rows . "</b></td></tr>";
                        } else {
                            echo "<td>Không tìm thấy tin thỏa mãn tiêu chí tìm kiếm.</td>";
                        }
                        require("closeconnection.php");
                        ?>
                    </tr>
                </table>

                <!--end body--></td>
            <td width="242" valign="top"><?php require("right.php"); ?></td>
        </tr>
        <tr>
            <td colspan="3" valign="top"><?php include("bottom.html"); ?></td>
        </tr>
    </table>
</div>
</body>
</html>
