<?php
session_start();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=utf-8"/>
    <title>Market To Home</title>
    <style type="text/css">
        <!--
        a {
            color: #3399FF
        }

        .topmenu {
            font-family: Arial, Helvetica, sans-serif;
            font-style: normal;
            color: #FFFFFF;
        }

        .style1 {
            color: #CC6600
        }

        .style2 {
            color: #0099FF
        }

        -->
    </style>
</head>

<body>
<div align="center">
    <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="3" valign="top"><?php require("top.php"); ?></td>
        </tr>
        <tr>
            <td width="200" valign="top"><?php require("left.php"); ?></td>
            <td width="548" valign="top"><!--begin body-->


                <?php
//                session_start();
                if (isset($_SESSION["UserId"]))
                    header("Location:myaccount.php");
                $message = "Enter your username and password:";
                if (isset($_SESSION["Wrong"])) {
                    $wrongCode = $_SESSION["Wrong"];
                    if ($wrongCode == "1") {
                        $message = "<font color='red'>User name is not existing, please enter again:</font>";
                    }
                    if ($wrongCode == "2") {
                        $message = "<font color='red'>Wrong password, please enter again:</font>";
                    }
                }
                ?>

                <form action='login-authentication.php' method='post'>
                    <?php
                    echo $message;
                    ?>
                    <br>
                    <table>
                        <tr>
                            <td>User name:</td>
                            <td><input type='text' name='user' required></td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td><input type='password' name='pass' required></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type='submit' name='submit' value='OK'><input type='reset' name='reset'
                                                                                     value='Reset'></td>
                        </tr>
                    </table>
                </form>


                <!--end body--></td>
            <td width="242" valign="top"><?php require("right.php"); ?></td>
        </tr>
        <tr>
            <td colspan="3" valign="top"><?php include("bottom.html"); ?></td>
        </tr>
    </table>
</div>
</body>
</html>
