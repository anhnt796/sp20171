<br>GIỎ HÀNG<br>
<?php
session_start();
if (isset($_SESSION["Cart"])) {
    //Destroy
    if (isset($_POST["destroy"])) {
        unset($_SESSION["Cart"]);
        echo "Xin lỗi, giỏ hàng của bạn bị rỗng.";
    } else {
        ?>
        <form name="cart" method="post" action="viewcart.php">
            <table border="1" width="96%" cellspacing="1" cellpadding="1">
                <tr>
                    <td>Mã</td>
                    <td>Tên</td>
                    <td>Số lượng</td>
                    <td>Giá</td>
                    <td>Thành tiền</td>
                </tr>
                <?php
                $shop = $_SESSION["Cart"];

                //Update
                if (isset($_POST["update"])) {

                    $qttycollection = $_POST["qtty"];
                    for ($i = 0; $i < count($qttycollection); $i++) {
                        $shop[$i]["Quantity"] = $qttycollection[$i];
                        $shop[$i]["Amount"] = $shop[$i]["Quantity"] * $shop[$i]["Price"];

                    }
                    $_SESSION["Cart"] = $shop;
                }
                //Delete
                if (isset($_POST["delete"])) {

                    $itemcollection = $_POST["itemid"];
                    for ($row = 0; $row < count($shop); $row++) {
                        for ($i = 0; $i < count($itemcollection); $i++) {
                            //echo "itemcollection".$itemcollection[$i]."<br>";
                            //echo "shop".$shop[$row]["Id"]."<br>";
                            if ($shop[$row]["Id"] == $itemcollection[$i]) {
                                unset($shop[$row]);
                                array_unshift($shop, array_shift($shop));
                            }
                        }
                    }
                    $_SESSION["Cart"] = $shop;
                }
                for ($row = 0; $row < count($shop); $row++) {
                    echo "<tr>";
                    echo "<td><input type=checkbox id='itemid' name='itemid[]' value=". $shop[$row]["Id"] . "></td>";
                    echo "<td>" . $shop[$row]["Title"] . "</td>";
                    echo "<td>";
                    echo "<input id='qtty' name='qtty[]' value='" . $shop[$row]["Quantity"]. "' size='3'></td>";
                    echo "<td>" . $shop[$row]["Price"] . "</td>";
                    echo "<td>" . $shop[$row]["Amount"] . "</td>";
                    echo "</tr>";
                }

                ?>
                <tr>
                    <td colspan="5">
                        <input name="update" type="submit" value="Cập nhật">
                        <input name="delete" type="submit" value="Xoá">
                        <input name="destroy" type="submit" value="Huỷ">
                        <input name="checkout" type="button" value="Đặt hàng"
                               onClick="window.open('checkout.php', target='_main');">
                    </td>
                </tr>
            </table>
        </form>
        <?php
    }
} else
    echo "Xin lỗi, giỏ hàng của bạn bị rỗng.";
?>