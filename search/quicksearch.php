<form name="searchform" method="post" action="searchresult.php">
    <table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr>
            <td width="13%">Từ khóa</td>
            <td width="15%"><label><input type="text" name="keyword" id="keyword"></label></td>
            <td width="13%">Loại sách</td>
            <td width="15%">
                <select name="category" id="category">
                    <?php
                    require("../environment/connection.php");
                    $select = "select * from Categories";
                    $result = mysqli_query($connect, $select);
                    echo "<option value=''>Chọn loại sách</option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<option value=";
                        echo $row["CategoryId"];
                        echo ">" . $row["CategoryName"];
                        echo "</option>";
                    }
                    require("../environment/closeconnection.php");
                    ?>
                </select>
            </td>
            <td><input type="submit" name="quick_submit" value="Tìm kiếm"></td>
        </tr>
        <input name="searchfrom" type="hidden" id="searchfrom" value="quick">
    </table>
</form>
<br>
<a href="basicsearch.php">Tìm kiếm cơ bản</a>
<br>
<a href="advancedsearch.php">Tìm kiếm nâng cao</a>