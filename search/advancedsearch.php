<form name="searchform" method="post" action="searchresult.php">
    <table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr>
            <td width="13%">Từ khóa</td>
            <td width="15%"><label><input type="text" name="keyword" id="keyword"></label></td>
            <td width="13%">Loại sách</td>
            <td width="15%">
                <select name="category" id="category">
                    <?php
                    require("../environment/connection.php");
                    $select = "select * from Categories";
                    $result = mysqli_query($connect, $select);
                    echo "<option value=''>Chọn loại sách</option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<option value=";
                        echo $row["CategoryId"];
                        echo ">" . $row["CategoryName"];
                        echo "</option>";
                    }
                    require("../environment/closeconnection.php");
                    ?>
                </select>
            </td>
            <td><input type="submit" name="advanced_submit" value="Tìm kiếm"></td>
        </tr>
        <tr>
            <td width="13%">Tác giả</td>
            <td width="15%">
                <select name="author" id="author">
                    <?php
                    require("../environment/connection.php");
                    $select = "select * from Authors";
                    $result = mysqli_query($connect, $select);
                    echo "<option value=''>Chọn tác giả</option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<option value=";
                        echo $row["AuthorId"];
                        echo ">" . $row["AuthorName"];
                        echo "</option>";
                    }
                    require("../environment/closeconnection.php");
                    ?>
                </select>
            </td>
            <td width="13%">Nhà XB</td>
            <td width="15%">
                <select name="publisher" id="publisher">
                    <?php
                    require("../environment/connection.php");
                    $select = "select * from Publishers";
                    $result = mysqli_query($connect, $select);
                    echo "<option value=''>Chọn nhà xuất bản</option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<option value=";
                        echo $row["PublisherId"];
                        echo ">" . $row["PublisherName"];
                        echo "</option>";
                    }
                    require("../environment/closeconnection.php");
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td width="13%">Loại độc giả</td>
            <td width="15%">
                <select name="readertype" id="readertype">
                    <?php
                    require("../environment/connection.php");
                    $select = "select * from ReaderTypes";
                    $result = mysqli_query($connect, $select);
                    echo "<option value=''>Chọn loại độc giả</option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<option value=";
                        echo $row["ReaderTypeId"];
                        echo ">" . $row["ReaderTypeName"];
                        echo "</option>";
                    }
                    require("../environment/closeconnection.php");
                    ?>
                </select>
            </td>
            <td width="13%">Ngôn ngữ</td>
            <td width="15%">
                <select name="language" id="language">
                    <?php
                    require("../environment/connection.php");
                    $select = "select * from Languages";
                    $result = mysqli_query($connect, $select);
                    echo "<option value=''>Chọn ngôn ngữ</option>";
                    while ($row = mysqli_fetch_array($result)) {
                        echo "<option value=";
                        echo $row["LanguageId"];
                        echo ">" . $row["LanguageName"];
                        echo "</option>";
                    }
                    require("../environment/closeconnection.php");
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td width="13%">Giá từ</td>
            <td width="15%"><label><input type="number" name="minprice" id="minprice"></label></td>
            <td width="13%">đến</td>
            <td width="15%"><label><input type="number" name="maxprice" id="maxprice"></label></td>
        </tr>
        <input name="searchfrom" type="hidden" id="searchfrom" value="advanced">
    </table>
</form>
<br>
<a href="quicksearch.php">Tìm kiếm nhanh</a>
<br>
<a href="basicsearch.php">Tìm kiếm cơ bản</a>