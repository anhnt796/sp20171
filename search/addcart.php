<?php
if (isset($_POST["itemid"])) {
    session_start();
    $id = $_POST["itemid"];
    $name = $_POST["itemname"];
    $price = $_POST["price"];
    $quantity = $_POST["quantity"];
    $amount = $price * $quantity;
    $isnew = true;
    $shop = null;
    if (isset($_SESSION["Cart"])) {

        $shop = $_SESSION["Cart"];
        $shop = IsAddOrUpdate($shop, $id, $name, $quantity, $price, $amount, $isnew);
    } else {

        $shop = array();
        $shop = array(array("Id" => $id, "Title" => $name, "Price" => $price, "Quantity" => $quantity, "Amount" => $amount));
    }

    $_SESSION["Cart"] = $shop;
    ?>

    Bạn đã <?php echo($isnew ? "thêm mới " : "cập nhật số lượng của ");
    echo "[" . $name . "]" ?> vào giỏ hàng.

    <?php
} else
    echo "Xin lỗi, bạn chưa chọn sản phẩm.";
echo "<br><a href='viewcart.php'>Xem giỏ hàng</a>";

function IsAddOrUpdate($shop, $id, $name, $quantity, $price, $amount, &$isnew)
{

    $numberofitems = count($shop);

    for ($i = 0; $i < $numberofitems; $i++) {

        if ($shop[$i]["Id"] == $id) {
            $shop[$i]["Quantity"] += $quantity;
            $shop[$i]["Amount"] = $shop[$i]["Quantity"] * $shop[$i]["Price"];
            $isnew = false;
            break;
        }

    }
    if ($isnew == true) {

        $shop[$numberofitems] = array("Id" => $id, "Title" => $name, "Price" => $price, "Quantity" => $quantity, "Amount" => $amount);
    }
    return $shop;
}

?>