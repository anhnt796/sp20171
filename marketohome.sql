-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2017 at 03:49 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketohome`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `AuthorId` int(11) UNSIGNED NOT NULL,
  `AuthorName` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`AuthorId`, `AuthorName`) VALUES
(1, 'Nguyễn Kim Khánh'),
(2, 'Nguyễn Đức Nghĩa');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CategoryId` int(11) UNSIGNED NOT NULL,
  `CategoryName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CategoryId`, `CategoryName`, `Description`) VALUES
(1, 'Programming Languages', 'Computer Programming Languages'),
(2, 'Software Solutions', 'Computer Software Solutions'),
(3, 'Databases', 'Computer Databases'),
(4, 'Hardware', 'Computer Hardware'),
(5, 'Web Development', 'Computer Web Development'),
(6, 'Web Design', 'Computer Web Design'),
(7, 'Operating System', 'Computer Operating System'),
(8, 'Microsoft Office', 'Computer Microsoft Office'),
(9, 'Languages', 'Computer Languages'),
(14, 'Analysis', 'Computer Analysis');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `CustomerID` int(11) NOT NULL,
  `CustomerName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Telephone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`CustomerID`, `CustomerName`, `Address`, `Telephone`, `Email`) VALUES
(1, 'Nguyễn Tuấn Anh', '41 Vũ Xuân Thiều, quận Long Biên, Hà Nội', '01689940629', 'anhnt796@gmail.com'),
(2, 'Anh Nguyễn Tuấn', '69 Vũ Xuân Thiều, quận Long Biên, Hà Nội', '0984291242', 'anhnt69@vnn.vn');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `ItemId` int(11) NOT NULL,
  `ItemName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Price` double NOT NULL,
  `Size` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Weight` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `CategoryId` int(11) UNSIGNED NOT NULL,
  `AuthorId` int(11) UNSIGNED NOT NULL,
  `PublisherId` int(11) UNSIGNED NOT NULL,
  `ReaderTypeId` int(11) UNSIGNED NOT NULL,
  `LanguageId` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`ItemId`, `ItemName`, `Price`, `Size`, `Weight`, `Description`, `CategoryId`, `AuthorId`, `PublisherId`, `ReaderTypeId`, `LanguageId`) VALUES
(1, 'Lập trình chuyên nghiệp SQL Server 2016 Tập 1', 60000, '320x520', '520', 'Databases, Nguyễn Kim Khánh, NXB Đại học Bách Khoa Hà Nội, Vietnamese, Student', 3, 1, 1, 1, 5),
(2, 'Lập trình chuyên nghiệp SQL Server 2016 Tập 2', 64000, '320x520', '540', 'Databases, Nguyễn Kim Khánh, NXB Đại học Bách Khoa Hà Nội, Vietnamese, Student', 3, 1, 1, 1, 5),
(3, 'Lập trình C cơ bản', 45000, '320x520', '300', 'Programming Languages, Nguyễn Đức Nghĩa, NXB Đại học Bách Khoa Hà Nội, English, Student', 1, 2, 1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `LanguageId` int(11) UNSIGNED NOT NULL,
  `LanguageName` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`LanguageId`, `LanguageName`) VALUES
(1, 'English'),
(2, 'French'),
(3, 'Japanese'),
(4, 'Chinese'),
(5, 'Vietnamese');

-- --------------------------------------------------------

--
-- Table structure for table `publishers`
--

CREATE TABLE `publishers` (
  `PublisherId` int(11) UNSIGNED NOT NULL,
  `PublisherName` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `publishers`
--

INSERT INTO `publishers` (`PublisherId`, `PublisherName`) VALUES
(1, 'NXB Đại học Bách Khoa Hà Nội');

-- --------------------------------------------------------

--
-- Table structure for table `readertypes`
--

CREATE TABLE `readertypes` (
  `ReaderTypeId` int(11) UNSIGNED NOT NULL,
  `ReaderTypeName` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `readertypes`
--

INSERT INTO `readertypes` (`ReaderTypeId`, `ReaderTypeName`) VALUES
(1, 'Student'),
(2, 'Engineer '),
(3, 'Kid'),
(4, 'Businessman');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `fullname`, `email`) VALUES
('tuananh', '666666', 'Nguyễn Tuấn Anh', 'anhnt796@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`AuthorId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`CustomerID`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`ItemId`),
  ADD KEY `FKForCategory` (`CategoryId`),
  ADD KEY `FKForAuthor` (`AuthorId`),
  ADD KEY `FKForPublisher` (`PublisherId`),
  ADD KEY `FKForReaderType` (`ReaderTypeId`),
  ADD KEY `FKForLanguage` (`LanguageId`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`LanguageId`);

--
-- Indexes for table `publishers`
--
ALTER TABLE `publishers`
  ADD PRIMARY KEY (`PublisherId`);

--
-- Indexes for table `readertypes`
--
ALTER TABLE `readertypes`
  ADD PRIMARY KEY (`ReaderTypeId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `AuthorId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `CategoryId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `ItemId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `LanguageId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `publishers`
--
ALTER TABLE `publishers`
  MODIFY `PublisherId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `readertypes`
--
ALTER TABLE `readertypes`
  MODIFY `ReaderTypeId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `FKForAuthor` FOREIGN KEY (`AuthorId`) REFERENCES `authors` (`AuthorId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKForCategory` FOREIGN KEY (`CategoryId`) REFERENCES `categories` (`CategoryID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKForLanguage` FOREIGN KEY (`LanguageId`) REFERENCES `languages` (`LanguageId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKForPublisher` FOREIGN KEY (`PublisherId`) REFERENCES `publishers` (`PublisherId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKForReaderType` FOREIGN KEY (`ReaderTypeId`) REFERENCES `readertypes` (`ReaderTypeId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
